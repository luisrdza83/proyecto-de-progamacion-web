
$(document).ready(function()
{
    // el cuadrito magenta iniciará siempre en el primero
    $('ul.navbar-nav li a:first').addClass('active');
    // los contenidos de los articulos no se mostraran
    $('.secciones article').hide();
    // solo mostrara el primero
    $('.secciones article:first').show();

    $('ul.navbar-nav li a').click(function()
    {
        // en esta funcion se vera que al momento de dar click en otra seccion
        // desactivará en el que está
        $('ul.navbar-nav li a').removeClass('active')
        // para ponerlo en el this que es el que se está presionando
        $(this).addClass('active');

        // el contenido del articulo en el que estaba se ocultará
        $('.secciones article').hide();
        // en una variable agarrará el href del que se esta presionando
        var activeTab = $(this).attr('href');
        // aqui vemos si está correcto el href que presionamos
        // console.log(activeTab);
        // mostramos el contenido que tiene la seccion
        $(activeTab).show();
        return false; 
    });

})